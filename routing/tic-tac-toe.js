import express from 'express';

var router = express.Router();

(function() {

var wins = {blue:0, red:0};

router.get('/wins', function(req, res) {
	console.log('/api/wins');
	res.json(wins);
});

router.post('/winsUpdate', function(req, res) {
	console.log('/api/winsUpdate');
	console.log(req.body);
	if (req.body.blue) {
		wins.blue += req.body.blue;
	}
	if (req.body.red) {
		wins.red += req.body.red;
	}

	res.json(wins);
})

})();

export default router;
