import express from 'express';
import tictactoe from './tic-tac-toe.js';

var router = express.Router();

router.use('/tictactoe', tictactoe);

/**
 * Error Handling
 */

router.use(function(req, res, next) {
  console.log('404')
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

router.use(function(err, req, res, next) {
  res.sendStatus(err.status || 500);
});

export default router;
