import React from 'react';

export default function(props) {
	return (
		<div>
			<span>Grid size: </span>
			<select value={props.value} onChange={props.onChange}>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
			</select>
		</div>
	);
}
