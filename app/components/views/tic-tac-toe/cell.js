import React from 'react';

export default class extends React.Component {
	constructor(props) {
		super(props);
	}

	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}

	render() {
		return (
			<div className={(this.props.player || '') + " cell"} onClick={this.props.onClick}></div>
		);
	}
}
