import React from 'react';

export default function(props) {
	if (props.result) {
		if (props.result != "draw") {
			var text = props.result + " won!";
		}
		else {
			var text = "It is a draw.";
		}
	}
	else {
		var text = "It is currently " + props.currentPlayer + "'s turn.";
	}
	return (
		<div id="turn-indicator">{text}</div>
	);
}
