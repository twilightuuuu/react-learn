import React from 'react';

export default class extends React.Component {
	constructor(props) {
		super(props);
	}

	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.wins.red !== this.props.wins.red || nextProps.wins.blue !== this.props.wins.blue;
	}

	render() {
		return (
			<div>
				<span>Red wins: {this.props.wins.red} Blue wins: {this.props.wins.blue}</span>
			</div>
		);
	}
}
