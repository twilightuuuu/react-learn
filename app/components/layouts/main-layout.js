import React from 'react';
import {Link} from 'react-router';

export default function(props) {
	return (
		<div id="app">
			<div id="layout-links">
				<li><Link to="/tic-tac-toe" activeClassName="active">Tic-tac-toe</Link></li>
				<li><Link to="/time" activeClassName="active">Time</Link></li>
			</div>
			<div id="layout-main">
				{props.children}
			</div>
		</div>
	);
}
