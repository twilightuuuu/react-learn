import React from 'react';
import {connect} from 'react-redux';
import store from '~/store';
import {moveAction, resetAction, changeGridSizeAction, getWinsAction} from '~/actions/tic-tac-toe-actions';
import TurnIndicator from '~/components/views/tic-tac-toe/turn-indicator';
import Cell from '~/components/views/tic-tac-toe/cell';
import SizeInput from '~/components/views/tic-tac-toe/size-input';
import WinCounter from '~/components/views/tic-tac-toe/win-counter';

class TicTacToeComponent extends React.Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		store.dispatch(getWinsAction());
	}

	resetGameState() {
		store.dispatch(resetAction());
	}

	render() {
		return (
			<div id="tictactoe">
				{this.props.grid.map(this.mapRow.bind(this))}
				<TurnIndicator currentPlayer={this.props.currentPlayer} result={this.props.gameResult} />
				<WinCounter wins={this.props.wins} />
				<SizeInput value={this.props.gridSize} onChange={this.changeGridSize}/>
				<input type="button" onClick={this.resetGameState.bind(this)} value="RESET"/>
			</div>
		);
	}

	mapCell(cell, colIndex, rowIndex) {
		return <Cell player={cell} key={rowIndex} onClick={this.cellClick.bind(this, colIndex, rowIndex)} />;
	}
	mapRow(row, colIndex) {
		return <div className="row" key={colIndex}>{row.map(function(cell, index){return this.mapCell(cell, colIndex, index)}.bind(this))}</div>;
	}

	cellClick(col, row) {
		store.dispatch(moveAction(col, row));
	}

	changeGridSize(event) {
		store.dispatch(changeGridSizeAction(event.target.value));
	}
}

const mapStateToProps = function(store) {
	return {
		wins: store.ticTacToeState.wins,
		grid: store.ticTacToeState.grid,
		gridSize: store.ticTacToeState.gridSize,
		currentPlayer: store.ticTacToeState.currentPlayer,
		gameResult: store.ticTacToeState.gameResult
	};
}

export default connect(mapStateToProps)(TicTacToeComponent);
