import React from 'react';

class TestComponent extends React.Component {
	constructor(props) {
			super(props);
			this.state = {
				time: new Date()
			};
	}

	render() {
		var dateRepr = this.state.time.toString();
		return (
			<p>The current time is: {dateRepr}</p>
		);
	}
}

export default TestComponent;
