import React from 'react';
import {Router, Route, browserHistory, IndexRoute} from 'react-router';
import MainLayout from '~/components/layouts/main-layout';
import TicTacToeComponent from '~/components/containers/tic-tac-toe-container';
import TimeComponent from '~/components/containers/time-container';

export default (
	<Router history={browserHistory}>
		<Route component={MainLayout}>
			<Route path="/"></Route>
			<Route path="tic-tac-toe" component={TicTacToeComponent}>
			</Route>
			<Route path="time" component={TimeComponent}>
			</Route>
		</Route>
	</Router>
);
