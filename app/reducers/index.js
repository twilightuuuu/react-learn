import { combineReducers } from 'redux';

import ticTacToeReducer from './tic-tac-toe-reducer';

var reducers = combineReducers({
	ticTacToeState: ticTacToeReducer
});

export default reducers;
