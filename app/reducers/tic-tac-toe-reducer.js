import * as types from '~/actions/action-types';

const initialGrid = function(gridSize) {
	var grid = new Array(gridSize);
	for (var i = 0 ; i < gridSize ; i++) {
		grid[i] = new Array(gridSize);
		for (var j = 0 ; j < gridSize ; j++) {
			grid[i][j] = false;
		}
	}
	return grid;
}

const newInitialState = function(gridSize, existingWins) {
	let newState = {
		gridSize,
		grid: initialGrid(gridSize),
		currentPlayer: 'blue',
		gameResult: false,
		winsToAdd: null,
		wins: {
			red: 0,
			blue: 0
		}
	};

	if (existingWins) {
		newState.wins = Object.assign({}, existingWins);
	}

	return newState;
}

const initialState = newInitialState(3, null);

const moveIsLegal = function(grid, col, row) {
	return !grid[col][row];
}

const gameResult = function(grid, gridSize) {
	// Check the rows.
	for (var row = 0 ; row < gridSize ; row++) {
		var color = grid[row][0];
		if (color) {
			for (var col = 1 ; col < gridSize ; col++) {
				if (grid[row][col] != color) {
					color = false;
					break;
				}
			}

			if (color) {
				return color;
			}
		}
	}

	// Check the columns.
	for (var col = 0 ; col < gridSize ; col++) {
		var color = grid[0][col];
		if (color) {
			for (var row = 1 ; row < gridSize ; row++) {
				if (grid[row][col] != color) {
					color = false;
					break;
				}
			}

			if (color) {
				return color;
			}
		}
	}

	// Check the 10 o'clock diagonal.
	var color = grid[0][0];
	if (color) {
		for (var index = 1 ; index < gridSize ; index++) {
			if (grid[index][index] != color) {
				color = false;
				break;
			}
		}

		if (color) {
			return color;
		}
	}

	// Check the 2 o'clock diagonal.
	var color = grid[0][gridSize-1];
	if (color) {
		for (var index = 1 ; index < gridSize ; index++) {
			if (grid[index][gridSize-index-1] != color) {
				color = false;
				break;
			}
		}

		if (color) {
			return color;
		}
	}

	// No win conditions were found, check for a draw.
	var isDraw = true;
	for (var row = 0 ; row < gridSize ; row++) {
		for (var col = 0 ; col < gridSize ; col++) {
			if(!grid[row][col]) {
				isDraw = false;
				break;
			}
		}
	}
	if (isDraw) {
		return "draw";
	}

	// The game is not finished.
	return false;
}

const copyGrid = function(grid) {
	var newGrid = [];
	for (var i = 0 ; i < grid.length ; i++) {
		newGrid.push(grid[i].slice(0));
	}

	return newGrid;
}

const updateGameStateByMove = function(state, col, row) {
	var newState = Object.assign({}, state, {
		grid: copyGrid(state.grid),
		wins: {red: state.wins.red, blue: state.wins.blue},
		winsToAdd: null
	});
	if (!newState.gameResult) {
		if (moveIsLegal(newState.grid, col, row)) {
			newState.grid[col][row] = newState.currentPlayer;

			newState.currentPlayer = (newState.currentPlayer == "red") ? "blue" : "red";
			newState.gameResult = gameResult(newState.grid, newState.gridSize);

			if (newState.gameResult) {
				console.log('gameJustEnded:reducer');
				newState.winsToAdd = {};
				newState.winsToAdd[newState.gameResult] = 1;
			}
		}
	}

	return newState;
}

const updateWins = function(state, wins) {
	return Object.assign({}, state, {
		grid: copyGrid(state.grid),
		wins: wins,
		winsToAdd: null
	});
}

const ticTacToeReducer = function(state = initialState, action) {
	switch(action.type) {
		case types.TICTACTOE_MOVE:
			return updateGameStateByMove(state, action.col, action.row);
		case types.TICTACTOE_RESET:
			return newInitialState(state.gridSize, state.wins);
		case types.TICTACTOE_CHANGEGRIDSIZE:
			if (!isNaN(action.gridSize))
				return newInitialState(action.gridSize, state.wins);
			else
				break;
		case types.TICTACTOE_GET_WINS_SUCCESS:
			return updateWins(state, action.wins);
	}

	return state;
}

export default ticTacToeReducer;
