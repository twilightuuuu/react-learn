import * as types from './action-types';
import * as tictactoeApi from '~/api/tictactoe-api';

function reset() {
	return {
		type: types.TICTACTOE_RESET
	};
}

function getWinsSuccess(wins) {
	return {
		type: types.TICTACTOE_GET_WINS_SUCCESS,
		wins
	}
}

function move(col, row) {
	return {
		type: types.TICTACTOE_MOVE,
		col,
		row
	};
}

function changeGridSize(gridSize) {
	return {
		type: types.TICTACTOE_CHANGEGRIDSIZE,
		gridSize: parseInt(gridSize, 10)
	}
}



export function moveAction(col, row) {
	return (dispatch, getState) => {
		dispatch(move(col, row));
		let state = getState();
		if (state.ticTacToeState.winsToAdd) {
			console.log('gameJustEnded:actions');
			tictactoeApi.addWins(state.ticTacToeState.winsToAdd)
				.then(response => {
					dispatch(getWinsSuccess(response.data));
				});
		}
	};
}

export function resetAction() {
	return dispatch => {
		dispatch(reset());
		dispatch(getWinsAction());
	};
}

export function changeGridSizeAction(gridSize) {
	return dispatch => {
		dispatch(changeGridSize(gridSize));
		dispatch(getWinsAction());
	};
}

export function getWinsAction() {
	return dispatch => {
		tictactoeApi.getWins()
			.then(response => {
				dispatch(getWinsSuccess(response.data));
			});
	}
}
