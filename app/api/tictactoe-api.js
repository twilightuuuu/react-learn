import axios from 'axios';
import store from '~/store';
import config from '~/api/config';
import {getWinsSuccess} from '~/actions/tic-tac-toe-actions';

export function getWins() {
	return axios.get(config.server + '/api/tictactoe/wins');
}

export function addWins(winsToAdd) {
	return axios.post(config.server + '/api/tictactoe/winsUpdate', winsToAdd);
}
