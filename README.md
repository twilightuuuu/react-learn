react-learn
===========

A toy server-client application created for personal React learning purposes.
Requires npm to be installed.

The client contains 2 sample features:

* Displaying the current time
* 2-player Tic-tac-toe with adjustable board size

## Installation

```js
npm install
```

## Usage

```js
npm run watch
```

After the build process ends, the webpage should be accessible at http://localhost:3000.