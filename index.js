'use strict';

var React = require('react');
var ReactDOM = require('react-dom');

class TestComponent extends React.Component {
	render() {
		var dateRepr = this.props.date.toString();
		return (
			<p>The current time is: {dateRepr}</p>
		);
	}
}

class TicTacToeCell extends React.Component {
	constructor(props) {
		super(props);
	}

	// handleStateChange() {
	// 	console.log("handleStateChange");
	// 	console.log(this.props.col);
	// 	this.props.changeState(this.props.col, this.props.row, !this.props.toggled)
	// }

	render() {
		return (
			<div className={(this.props.player || '') + " cell"} onClick={this.props.onClick}></div>
		);
	}
}

class TicTacToeTurnIndicator extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		if (this.props.result) {
			if (this.props.result != "draw") {
				var text = this.props.result + " won!";
			}
			else {
				var text = "It is a draw.";
			}
		}
		else {
			var text = "It is currently " + this.props.currentPlayer + "'s turn.";
		}
		return (
			<div id="turn-indicator">{text}</div>
		);
	}
}

class TicTacToeComponent extends React.Component {
	constructor(props) {
		super(props);
		this.gridSize = 3;
		this.state = this.initGameState();

		this.mapCell = this.mapCell.bind(this);
		this.mapRow = this.mapRow.bind(this);
	}

	initGameState() {
		var grid = new Array(this.gridSize);
		for (var i = 0 ; i < this.gridSize ; i++) {
			grid[i] = new Array(this.gridSize);
			for (var j = 0 ; j < this.gridSize ; j++) {
				grid[i][j] = false;
			}
		}

		return {
			grid: grid,
			currentPlayer: 'blue',
			gameResult: false
		};
	}

	resetGameState() {
		this.setState(this.initGameState());
	}

	render() {
		return (
			<div id="tictactoe">
				{this.state.grid.map(this.mapRow)}
				<TicTacToeTurnIndicator currentPlayer={this.state.currentPlayer} result={this.state.gameResult} />
				<input type="button" onClick={this.resetGameState.bind(this)} value="RESET"/>
			</div>
		);
	}

	mapCell(cell, colIndex, rowIndex) {
		return <TicTacToeCell player={cell} key={rowIndex} changeState={this.cellClickCallback} onClick={this.cellClick.bind(this, colIndex, rowIndex)} />;
	}
	mapRow(row, colIndex) {
		return <div className="row" key={colIndex}>{row.map(function(cell, index){return this.mapCell(cell, colIndex, index)}.bind(this))}</div>;
	}

	moveIsLegal(col, row) {
		return !this.state.grid[col][row];
	}

	getUpdatedGameState(col, row) {
		var nextPlayer = (this.state.currentPlayer == "red") ? "blue" : "red";
		var grid = this.state.grid;
		var gameResult = this.gameResult(grid);

		return {
			currentPlayer: nextPlayer,
			grid: grid,
			gameResult: gameResult
		};
	}

	gameResult(grid) {
		// Check the rows.
		for (var row = 0 ; row < this.gridSize ; row++) {
			var color = grid[row][0];
			if (color) {
				for (var col = 1 ; col < this.gridSize ; col++) {
					if (grid[row][col] != color) {
						color = false;
						break;
					}
				}

				if (color) {
					return color;
				}
			}
		}

		// Check the columns.
		for (var col = 0 ; col < this.gridSize ; col++) {
			var color = grid[0][col];
			if (color) {
				for (var row = 1 ; row < this.gridSize ; row++) {
					if (grid[row][col] != color) {
						color = false;
						break;
					}
				}

				if (color) {
					return color;
				}
			}
		}

		// Check the 10 o'clock diagonal.
		var color = grid[0][0];
		if (color) {
			for (var index = 1 ; index < this.gridSize ; index++) {
				if (grid[index][index] != color) {
					color = false;
					break;
				}
			}

			if (color) {
				return color;
			}
		}

		// Check the 2 o'clock diagonal.
		var color = grid[0][this.gridSize-1];
		if (color) {
			for (var index = 1 ; index < this.gridSize ; index++) {
				if (grid[index][this.gridSize-index-1] != color) {
					color = false;
					break;
				}
			}

			if (color) {
				return color;
			}
		}

		// No win conditions were found, check for a draw.
		var isDraw = true;
		for (var row = 0 ; row < this.gridSize ; row++) {
			for (var col = 0 ; col < this.gridSize ; col++) {
				if(!grid[row][col]) {
					isDraw = false;
					break;
				}
			}
		}
		if (isDraw) {
			return "draw";
		}

		// The game is not finished.
		return false;
	}

	cellClick(col, row) {
		if (!this.state.gameResult) {
			console.log("handleStateChange");
			if (this.moveIsLegal(col, row)) {
				var grid = this.state.grid;
				grid[col][row] = this.state.currentPlayer;
				this.setState(this.getUpdatedGameState())
			}
		}
	}
}


ReactDOM.render(
	<TestComponent date={new Date()} />,
	document.getElementById('container')
);

ReactDOM.render(
	<TicTacToeComponent />,
	document.getElementById('tictactoe-container')
);
